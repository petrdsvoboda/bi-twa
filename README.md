# HTML5 analog clock

Created for class BI-TWA by Petr Svoboda

Uses html canvas element to draw analog clock

Clock face is inspired by vintage clocks. It features hour, minute, second and millisecond hands with smoth movement and AM/PM indicator.

## Important files

- index.html - Landing page
- style.css - Application styling
- script.js - Application logic

Application uses custom font Playfair Display loaded from Google fonts, but defaults to serif if font can't be accessed.