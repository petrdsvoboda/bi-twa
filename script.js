// Used to convert decimal number to Roman numerals
const ROMAN_NUMBER = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', ]

class Hand {
	// Constructs clock hand with x, y center of width/height sized clock
	// Type either hour, minute, second, millisecond
	constructor(x, y, width, height, type) {
        this.x = x
        this.y = y
        this.width = width
        this.height = height
        this.type = type
    }

	// Gets radian position of hand depending on its type	
    getPosition(type) {
        const time = new Date()
        if(type === 'millisecond') {
            return this.getMilliseconds(time)
        } else if(type === 'second') {
            return this.getSeconds(time)
        } else if(type === 'minute') {
            return this.getMinutes(time)
        } else if(type === 'hour') {
            return this.getHours(time)
        } else {
            return 0
        }
    }

	// Gets radian hour clock hand position
	// First gets it main position between 12 steps on the clock
	// To that it adds subposition between steps
    getHours(time, modifier = 12) {
		const position = ((2 * Math.PI) / modifier) * (time.getHours() % 12)
		const subposition = this.getMinutes(time, modifier * 60)
        return position + subposition
    }

	// Gets radian minute clock hand position
	// First gets it main position between 60 steps on the clock
	// To that it adds subposition between steps
    getMinutes(time, modifier = 60) {
		const position = ((2 * Math.PI) / modifier) * time.getMinutes()
		const subposition = this.getSeconds(time, modifier * 60)
        return position + subposition
    }

	// Gets radian second clock hand position
	// First gets it main position between 60 steps on the clock
	// To that it adds subposition between steps
    getSeconds(time, modifier = 60) {
		const position = ((2 * Math.PI) / modifier) * time.getSeconds()
		const subposition =  this.getMilliseconds(time, modifier * 1000)
        return position + subposition
    }

	// Gets radian millisecond clock hand position
	// Gets it main position between 1000 steps on the clock
    getMilliseconds(time, modifier = 1000) {
		const position = ((2 * Math.PI) / modifier) * time.getMilliseconds()
        return position
    }

	// Renders clock hand
    render(context) {
        context.save()
		
		// Translates to center and gets hand position
		context.translate(this.x, this.y)
		context.rotate(Math.PI)
		context.rotate(this.getPosition(this.type))

		if(this.type === 'x') {
			this.renderHour(context)
		} else {
			context.fillRect(0 - (this.width / 2), 0, this.width, this.height)
		}
        
        context.restore()
	}
	
	// Renders fancy hour clock hand
	// UNUSED
	renderHour(context) {
		context.beginPath()
		context.moveTo(0, 2)
		context.bezierCurveTo(0, 2, 100, 2, 100, 2)
		context.bezierCurveTo(100, 2, 100, -4, 100, -2)
		context.bezierCurveTo(100, -2, 0, -2, 0, -2)
		context.fill()
	}
}

class Dial {
	// Constructs clock hand with x, y center of clock with radius
    constructor(x, y, radius) {
        this.x = x
        this.y = y
        this.radius = radius
    }

	// Renders minute and second scales and AM/PM text
    render(context) {
		this.renderMinuteScale(context)
		this.renderSecondScale(context)
		this.renderHours(context)
	}

	// Renders minute scale with roman numbers for main clock hands
	renderMinuteScale(context) {
		// Drwas two circles as scale border
        context.beginPath()
        context.arc(this.x, this.y, this.radius * 0.9, 0, Math.PI * 2)
        context.stroke()
        context.beginPath()
        context.arc(this.x, this.y, this.radius * 0.87, 0, Math.PI * 2)
		context.stroke()
		
		context.save()
		
		context.translate(this.x, this.y)
		// 12 main lines with 4 smaller lines between
		const PARTS = 12 * 5
		for(let i = 0; i < PARTS; i++) {
            const angle = Math.PI * 2 * i / PARTS
			context.save()
			// Rotate to line
            context.rotate(angle)
			
			// Each fifth draw main line and number of seconds
			if(i % 5 === 0) {
				context.save()
				context.font = this.radius * 0.06 + 'px Playfair Display'
				context.textBaseline= 'middle'
				context.textAlign= 'center'
				context.rotate(Math.PI)	// Start at top
				context.translate(0, this.radius * 0.95) // Shift center between frame and scale
				context.rotate(Math.PI)	// Rotate number to face with bottom to the center
				context.fillText(i ? i : 60, 0, 0) // If 0 replace with 60 
				context.restore()

				// Set main line width
				context.lineWidth = 4
			}
			
			// Draw line on scale
			context.beginPath()
			context.moveTo(0, this.radius * 0.9)
			context.lineTo(0, this.radius * 0.87)
			context.stroke()

			context.restore()
		}
		
        context.restore()
	}

	renderSecondScale(context) {
		context.save()

		// Center under main clock center
		context.translate(this.x, this.y + this.radius * 0.4)

		context.save()

		// Draw hand pin
		context.fillStyle = 'black'
        context.beginPath()
        context.arc(0, 0, 2, 0, Math.PI * 2)
        context.fill()

		context.restore()

		// 12 main lines with 4 smaller lines between
		const PARTS = 12 * 5
		for(let i = 0; i < PARTS; i++) {
            const angle = Math.PI * 2 * i / PARTS
            context.save()
			// Rotate to line
            context.rotate(angle)
			
			// Draw line, make each fifth bigger
			context.beginPath()
			context.moveTo(0, this.radius * 0.2)
			if(i % 5 === 0) {
				context.lineTo(0, this.radius * 0.18)
			} else {
				context.lineTo(0, this.radius * 0.19)
			}
			context.stroke()

			context.restore()
		}
		
        context.restore()
	}
	
	renderHours(context) {
        context.save()

		// Transalte to center
        context.translate(this.x, this.y)
        
        context.font = this.radius * 0.2 + 'px Playfair Display'
        context.textBaseline= 'middle'
        context.textAlign= 'center'

        for(let i = 0; i < 12; i++) {
            const angle = Math.PI * 2 * (i + 1) / 12
            context.save()
            context.rotate(Math.PI)	// Start at top
            context.rotate(angle) // Rotate to hour
            context.translate(0, this.x * 0.76) // Move to hour position
            context.rotate(Math.PI) // Rotate number to face with bottom to the center
            context.fillText(ROMAN_NUMBER[i], 0, 0) // Translate to Roman numeral
            context.restore()
        }

        context.restore()
	}
}

class Clock {
	// Construct clock with x, y center and radius
    constructor(x, y, radius) {
        this.x = x
        this.y = y
		this.radius = radius
		// Create hands
        this.hands = [
            new Hand(x, y, 8, 200, 'hour'),
            new Hand(x, y, 4, 300, 'minute'),
            new Hand(x, y, 2, 300, 'second'),
            new Hand(x, y + (radius * 0.4), 1, 50, 'millisecond') // Center under clock center
        ]
        this.dial = new Dial(x, y, radius)
    }

    render(context) {
		// Frame
		context.save()

		context.fillStyle = 'black'
        context.beginPath()
        context.arc(this.x, this.y, this.radius, 0, Math.PI * 2)
        context.fill()
		context.fillStyle = 'white'
        context.beginPath()
        context.arc(this.x, this.y, this.radius - 10, 0, Math.PI * 2)
        context.fill()
		
		context.restore()

		// Hands
        for(const hand of this.hands) {
            hand.render(context)
        }

		// Dial
		this.dial.render(context)
		
		this.renderAm(context)
		
		// Middle pin
		context.save()

		context.fillStyle = 'black'
        context.beginPath()
        context.arc(this.x, this.y, 10, 0, Math.PI * 2)
        context.fill()
		context.fillStyle = 'white'
        context.beginPath()
        context.arc(this.x, this.y, 5, 0, Math.PI * 2)
		context.fill()

		context.restore()
	}
	
	// Renders text AM/PM to the right of center of clock
	renderAm(context) {
		const time = new Date()
		
		context.save()
		
        context.font = this.radius * 0.06 + 'px Playfair Display'
        context.textBaseline= 'middle'
		context.textAlign= 'center'

		context.translate(this.x, this.y)
		context.translate(this.radius * 0.4, 0)
		context.fillText(time.getHours() > 12 ? 'PM' : 'AM', 0, 0)

		context.restore()
	}
}

class Canvas {
	// Constructs canvas, assigns context and creates clock
    constructor(element) {
        this.element = element
		this.context = element.getContext('2d')
		
		// this.calculateSize()

        this.centerX = this.element.width / 2
        this.centerY = this.element.height / 2
        this.clock = new Clock(this.centerX, this.centerY, this.centerX)

        this.render = this.render.bind(this)
    }

	// Renders a frame of canvas
    render() {
		// this.calculateSize()

		// Reset canvas
        this.context.clearRect(0, 0, this.element.width, this.element.height)

        this.clock.render(this.context)

		// Smooth rendering
        window.requestAnimationFrame(this.render)
	}
	
	calculateSize() {
		const documentHeight = document.body.clientHeight * 0.9
		this.height = this.element.height = documentHeight
		this.width = this.element.width = documentHeight
		this.radius = documentHeight
	}
}

// Gets html canvas element
const root = document.getElementById('root')
const canvas = new Canvas(root)

// Begins animation
window.requestAnimationFrame(canvas.render)